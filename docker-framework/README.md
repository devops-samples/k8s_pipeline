## Create a template Docker image
• Creates a base Docker image called ‘docker-framework’ using Ubuntu + Java + Tomcat
• Pushes it to a local repository in Artifactory

### Step to create Jenkins Pipeline:
<b>Note:</b> List of required Jenkins plugins
*   [Artifactory Plugin](https://wiki.jenkins.io/display/JENKINS/Artifactory+Plugin)   
*   [Docker Pipeline Plugin](https://wiki.jenkins.io/display/JENKINS/Docker+Pipeline+Plugin)   
*   [GitHub plugin](https://plugins.jenkins.io/git)   
*   [Pipeline Github Plugin](https://wiki.jenkins.io/display/JENKINS/Pipeline+Github+Plugin)   
*   [Pipeline Plugin](https://wiki.jenkins.io/display/JENKINS/Pipeline+Plugin)   

1.  On the Jenkins front page, click on Credentials -> System -> Global credentials -> Add Credentials
    Add your Artifactory credentials as the type Username with password, with the ID artifactory-credentials 
    ![Add_Artifactory_Credentials](../images/Add_Credentials.png)
    
2.  Create Following Docker repositories in Artifactory.
    * `docker-dev-local`  - Local docker repo.
    * `docker-lib-remote` - Remote docker repo pointing to Docker hub `https://registry-1.docker.io/`.
    * `docker-lib-virtual`- Virtual docker repo including docker-lib-remote
    * `generic-lib-local` - Local Generic repo.
    
3.  Deploy [Java](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) and [Tomcat](https://archive.apache.org/dist/tomcat/tomcat-8/v8.0.32/bin/apache-tomcat-8.0.32.tar.gz) binaries in generic-lib-local repo. 

4.  Create new Jenkins Pipeline Job.

5.  Add String Parameters:
    *   ARTI_DOCKER_REGISTRY (String Parameter) : Domain of Artifactory docker registry 
		e.g `ARTI_DOCKER_REGISTRY : docker.artifactory`
    *   DOCKER_DEV_REPO (String Parameter) -> Artifactory local docker registry, to deploy docker-framework images<Br>
		e.g.  `DOCER_DEV_REPO -> docker-dev-local`
    *   DOCKER_LIB_REPO (String Parameter) : Artifactory virtual docker registry, to pull base image from docker hub.<Br>
	    e.g. `DOCKER_LIB_REPO -> docker-lib-virtual`
    *   GEN_LIB_REPO (String Parameter) : Artifactory local Generic repo, to get packages for docker build.<Br>
	    e.g. `GEN_LIB_REPO -> generic-lib-local`
    *   XRAY_SCAN (Choice Parameter) : Xray Scan. Applicable only if you are using JFrog Xray<Br>
        e.g. `XRAY_SCAN -> YES`
    
6.  Copy [Jenkinsfile](Jenkinsfile) to Pipeline Script.

7.  To build it, press Build Now.

8.  Check your newly published build in build browser of Artifactory.
